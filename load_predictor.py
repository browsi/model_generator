# coding: utf-8
import numpy as np
import matplotlib 
matplotlib.use('Agg')
def parse_data():
    import os
    import Parse_all_data
    data = Parse_all_data.get_data(method=config.loading_method)
    print('data loaded')
    if config.sample:
        sample_size=config.sample_size
        data=data.sample(sample_size)
    print('parsing page structue')
    data = data.apply(lambda row: Parse_all_data.parse_page_element_list(row, config.element_list), axis=1)
    data = Parse_all_data.add_missing_elements(data, config.element_list)
    data=data.dropna(subset=['browsi_spot_offset_and_height'])
    print('calculating distances')
    data = data.apply(lambda row: Parse_all_data.calculate_distances(row, config.element_list), axis=1)
    print('moving redundant element columns')
    data = Parse_all_data.drop_redundant_element_columns(data, config.element_list)
    print('adding viewability features')
    data = Parse_all_data.add_viewability_to_features(data)
    print('filling NAs')
    data = Parse_all_data.fill_na_element_columns(data, config.element_list)
    data = Parse_all_data.fill_na_num_of_element_columns(data, config.num_of_element_list)
    data = Parse_all_data.fill_empty_velocity(data, config.velocity_columns)
    print('parsing time')
    data = data.apply(Parse_all_data.parse_time, axis=1, reduce=True)
    print('adding dummy columns')
    print('removing unwanted columns')
    data = Parse_all_data.drop_unwanted_columns(data, config.drop_column_list)
    data = Parse_all_data.add_dummy_columns(data)
    data = Parse_all_data.add_missing_dummy_columns(data)
    print('unwanted column removed')
    # From now on the data manipulation is not independent between train/test/dev
    data_sets = {}
    if config.validate == True:
        train, validation, test = Parse_all_data.split_test_train(data, include_validation=True)
        data_sets['train'] = train
        data_sets['validation'] = validation
        data_sets['test'] = test
    else:
        train, test = Parse_all_data.split_test_train(data, include_validation=False)
        data_sets['train'] = train
        data_sets['test'] = test



    for key in data_sets.keys():
        data = data_sets[key]
        data = Parse_all_data.fill_na_with_mean(data, config.numerical_columns)
        print('all NAs filled with mean')
        if config.standartize:
            print('standarizing data')
            data = Parse_all_data.standarize_numerical_colunms(data, config.numerical_columns, method='robust')
        data = Parse_all_data.remove_remaining_na(data)
        data_sets[key] = data

    for key in data_sets.keys():
        X, Y = Parse_all_data.split_X_Y(data_sets[key])
        if config.balance and key=='train':
            X, Y = Parse_all_data.balance_data(X, Y, type='combined')
            print('data balanced')
        if config.PCA:
            X = Parse_all_data.PCA(X)
            print('PCA finished')
        data_sets[key] = Parse_all_data.recombine(X, Y)
        cwd = os.getcwd()
        data_sets[key].to_csv(cwd + '/' + 'parsed_' + key + config.site_key + '.csv', index=False)



    print(data_sets)
    print('saved parsed data')


def get_parsed_data(target='is_viewed'):
    import pandas as pd
    import os
    cwd=os.getcwd()
    data = pd.read_csv(cwd + '/' +'parsed_'+'train'+ config.site_key+ '.csv')
    X_train = data.ix[:, data.columns != target]
    Y_train = data[target]
    if config.validate:
        data = pd.read_csv(cwd + '/' +'parsed_'+'validation'+config.site_key+ '.csv')
        X_validation = data.ix[:, data.columns != target]
        Y_validation = data[target]

    else:
        X_validation = None
        Y_validation = None


    data=pd.read_csv(cwd + '/' +'parsed_'+'test'+ config.site_key+'.csv')
    X_test = data.ix[:, data.columns != target]
    Y_test = data[target]

    return X_train, Y_train, X_validation, Y_validation, X_test, Y_test

def eliminate_columns_after_data_fetch(X_train, Y_train, X_validation, Y_validation, X_test, Y_test):
    if config.validate:
        X_train=X_train[config.keep_cols]
        Y_train=Y_train
        X_validation=X_validation[config.keep_cols]
        Y_validation=Y_validation
        X_test=X_test[config.keep_cols]
        Y_test=Y_test
    else:
        X_train=X_train[config.keep_cols]
        Y_train=Y_train
        X_validation = None
        Y_validation = None
        X_test=X_test[config.keep_cols]
        Y_test=Y_test
    return  X_train, Y_train, X_validation, Y_validation, X_test, Y_test


def get_model(X,Y):
    import config
    model_num=config.model
    parameters=config.params_dict[model_num]
    import Create_classifiers
    if model_num==1:
        clf = Create_classifiers.GardientBoostedRandomForest(X, Y, parameters)
    elif model_num==2:
        clf= Create_classifiers.KNN(X,Y,parameters)
    elif model_num==3 and config.PCA:
        clf, clf_redundant =Create_classifiers.NaiveBayes(X, Y, [],X.columns)
    elif model_num==4:
        clf=Create_classifiers.CostSensitiveRandomForest(X,Y, parameters)
    elif model_num==5:
        clf=Create_classifiers.RandomForest(X,Y, parameters)
    elif model_num==6:
        clf=Create_classifiers.ExtremeRandomForest(X,Y, parameters)
    elif model_num==7:
        clf=Create_classifiers.SVC(X,Y, parameters)
    elif model_num==8:
        clf=Create_classifiers.Logistic_Regression(X,Y, parameters)
    return clf





def push_model_to_s3(clf):
    import boto3
    import config
    from sklearn.externals import joblib
    bucket = config.bucket_for_pkl
    key=config.key_for_pkl
    try:
        joblib.dump(clf, key)
        client_s3 = boto3.client('s3', aws_access_key_id=config.AWS_ACCESS_KEY_ID, aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
                                 region_name=config.REGION_NAME)
        client_s3.upload_file(Bucket=bucket, Key=key, Filename=key)
        print('Uploaded successfuly')
    except:
        print('Problem with file')

def push_features_to_s3(X):
    import boto3
    import config
    from sklearn.externals import joblib
    bucket = config.bucket_for_pkl
    key=config.key_for_feature_list
    try:
        list = X.columns.tolist()
        thefile = open(key, 'w')
        for item in list:
            thefile.write("%s\n" % item)

        thefile.close()
        client_s3 = boto3.client('s3', aws_access_key_id=config.AWS_ACCESS_KEY_ID, aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
                                 region_name=config.REGION_NAME)
        client_s3.upload_file(Bucket=bucket, Key=key, Filename=key)
        print('Uploaded successfuly')
    except:
        raise
        print('Problem with file')

def test_model(clf, X_validation, Y_validation, X_test, Y_test, validate=False):
    import Check_classifiers
    if validate:
        Y_predict = Check_classifiers.Predict(X_validation, clf)
        Y_proba = Check_classifiers.Predict_proba(X_validation, clf)
        acc = Check_classifiers.accuracy(Y_validation, Y_predict)
        auc = Check_classifiers.auc(Y_validation, Y_proba)
        logloss = Check_classifiers.log_loss(Y_validation, Y_proba)
        classification = Check_classifiers.gen_classification_report(Y_validation, Y_predict)
        brier = Check_classifiers.brier_scrore(Y_validation, Y_proba)
        ROC = Check_classifiers.ROC_curve(Y_validation, Y_proba)
        Reliability = Check_classifiers.reliability_curve(Y_validation, Y_proba)
    else:
        Y_predict = Check_classifiers.Predict(X_test, clf)
        Y_proba = Check_classifiers.Predict_proba(X_test, clf)
        acc = Check_classifiers.accuracy(Y_test, Y_predict)
        auc = Check_classifiers.auc(Y_test, Y_proba)
        logloss = Check_classifiers.log_loss(Y_test, Y_proba)
        classification = Check_classifiers.gen_classification_report(Y_test, Y_predict)
        brier = Check_classifiers.brier_score(Y_test, Y_proba)
        ROC = Check_classifiers.ROC_curve(Y_test, Y_proba)
        Reliability = Check_classifiers.reliability_curve(Y_test, Y_proba)


    print('acc is   ', acc)
    print('auc is   ', auc)
    print('log loss is   ', logloss)
    print('brier score is ', brier)
    print(classification)
    



if  __name__=='__main__':
    import config
    if config.parse_data:
        parse_data()

    X_train, Y_train, X_validation, Y_validation, X_test, Y_test=get_parsed_data()
    if config.eliminate_cols_after_data_fetch:
        X_train, Y_train, X_validation, Y_validation, X_test, Y_test=eliminate_columns_after_data_fetch(X_train, Y_train, X_validation, Y_validation, X_test, Y_test)
    if config.grid_search:
        from Create_classifiers import GridSerach
        full_results,best_estimator,best_params,best_score=GridSerach(config.grid_serach_classifier, config.grid_serach_params, X_train, Y_train)
        print(best_params,best_score)
    if not config.grid_search:
        X_train['viewability_per_user']=X_train['viewability_per_user'].astype('float64')
        clf=get_model(X_train,Y_train)
        #data_frame=np.std((X_test, axis=0)/np.std(X_test, axis=0))
        #print((clf.feature_importances_ * data_frame).sort_values(ascending=False))
        test_model(clf, X_validation, Y_validation, X_test, Y_test, validate=False)
    if config.push_model:
        push_model_to_s3(clf)
        push_features_to_s3(X_train)




