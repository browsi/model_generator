# coding: utf-8
import pandas as pd
import numpy as np
import config


validate=config.validate
element_list=config.element_list
num_of_element_list=config.num_of_element_list
numerical_columns=config.numerical_columns
drop_column_list=config.drop_column_list
categorical_columns=config.categorical_columns

def get_data(method='manual'):
    import pandas as pd
    import config
    if method=='manual':
        import os
        cwd = os.getcwd()
        path=cwd+'/'+config.file_name
        data=pd.read_csv(path)
        return data
    else:
        import boto3
        import botocore


        s3=boto3.resource('s3', aws_access_key_id=config.AWS_ACCESS_KEY_ID, aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
                                 region_name=config.REGION_NAME)
        try:
            s3.Bucket(config.bucket).download_file(config.key, config.path)
            print('file downloaded')
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

        try:
            data=pd.read_csv(config.path)
        except UnicodeDecodeError:
            data=pd.read_csv(config.path, encoding="ISO-8859-1")

        return data

def parse_page_element_list(Series,element_list):

    for element in element_list:
        Series[element]=eavl_string_to_list(Series[element])
        for spot in Series['browsi_spots_array']:
            if spot[0]==Series['ad_index']: #find the correct spot in the browsi_spots_array
                Series['browsi_spot_offset_and_height']=(spot[1],spot[2]) #get offset and higeht. remember each row of the dataset represents one spot


    #print('Parsing row', Series)
    return Series


def eavl_string_to_list(str):
    try:
        str=str.replace('null','None')
        lst=eval(str)  #evaluates string
        lst_filtered=[x for x in lst if isinstance(x[0], int) and isinstance(x[1], int)] #remove all Nones from string
    except AttributeError:
        lst_filtered=[]
    return lst_filtered




def calculate_distances (Series,element_list,exclude_element='browsi_spots_array', browsi_location_column='browsi_spot_offset_and_height'):
    import numpy as np
    elements=[x for x in element_list if x != exclude_element]
    browsi_spot_location = Series[browsi_location_column][0]
    browsi_spot_height = Series[browsi_location_column][1]
    for element in elements:
        distances=[]
        if element==[] or element is None:
            Series['distance_from_'+element]=-1
            continue
        for item in Series[element]:
            element_location = item[0]
            element_height = item[1]
            if element_location<=browsi_spot_location:
                distance=abs(browsi_spot_location-(element_location+element_height))
            else:
                distance=abs(element_location-(browsi_spot_location+browsi_spot_height))

            distances.append(distance)

        if distances != [] and distances is not None:
            Series['distance_from_'+element]=np.array(distances).min()
    Series['ad_location']=browsi_spot_location
    return Series


def add_missing_elements(DataFrame, element_list, exclude_element='browsi_spots_array'): #if an element is missing then add it with -1
    for element in element_list:
        column='distance_from_'+element
        if column not in DataFrame.columns and element!=exclude_element:
            DataFrame[column]=-1
    return DataFrame

def drop_redundant_element_columns(DataFrame,element_list,browsi_location_column='browsi_spot_offset_and_height'):
    for element in element_list:
        DataFrame.drop(element,axis=1, inplace=True)
    DataFrame.drop(browsi_location_column, axis=1, inplace=True)
    return DataFrame

def fill_na_element_columns(DataFrame,element_list,exclude_element='browsi_spots_array'):
    for element in element_list:
        if element != exclude_element:
            DataFrame['distance_from_'+element].fillna(-1, inplace=True)
    return DataFrame

def fill_na_num_of_element_columns(DataFrame,num_of_element_list):
    import numpy as np
    for element in num_of_element_list:
        DataFrame[element].fillna(0, inplace=True)
        DataFrame[element].round(0)
    return DataFrame


def parse_time(Series, geo_location='geo_location', now='now', published_date='published_date'):
    from datetime import datetime
    from pytz import country_timezones
    import pytz

    if not isinstance(Series['geo_location'], str) or Series['geo_location'] == 'ROW':
        country = 'us'
    else:
        country = Series['geo_location']

    time_zone = country_timezones(country)[0]  # get time zone of user's geo as a string representation
    time_zone = pytz.timezone(time_zone)  # convert to tmzn class
    now_unix = Series['now']
    publish_date = Series['published_date']

    if Series['geo_location'] not in ['us', 'br', 'gb']:  # group countries
        Series['geo_location'] = 'ROW'
    try:
        parsed_value = datetime.fromtimestamp(now_unix)

    except OSError:
        print('bad convertion')
        return None
    try:
        parsed_value = time_zone.localize(parsed_value, is_dst=None)

    except TypeError:
        parsed_value = parsed_value.astimezone(pytz.utc)
        raise

    Series['Day'] = parsed_value.day
    Series['Hour'] = parsed_value.hour
    if parsed_value.isoweekday() == 6 | parsed_value.isoweekday() == 7:
        Series['is_weekend'] = 1
    else:
        Series['is_weekend'] = 0

    Series['Weekday'] = parsed_value.isoweekday()

    if Series['Hour'] >= 0 and Series['Hour'] < 6:
        Series['Hour_category'] = 'night'

    elif Series['Hour'] >= 6 and Series['Hour'] < 12:
        Series['Hour_category'] = 'morning'

    elif Series['Hour'] >= 12 and Series['Hour'] < 18:
        Series['Hour_category'] = 'noon'

    else:
        Series['Hour_category'] = 'evening'

    if not isinstance(publish_date, str):
        Series['hours_since_published']=24
        #print(Series)

        return Series

    try:
        publish_date_unix = datetime.strptime(publish_date, '%Y-%m-%d %H:%M:%S %Z').timestamp()
    except ValueError:
        try:
            publish_date_unix = datetime.strptime(publish_date, '%Y-%m-%d').timestamp()
        except ValueError:
            try:
                publish_date_unix = datetime.strptime(publish_date, '%m/%d/%Y').timestamp()
            except ValueError:
                try:
                    publish_date_unix = datetime.strptime(publish_date[0:9], '%Y-%m-%d').timestamp()
                except ValueError:
                    publish_date_unix = Series['now'] - 86400  # exactly 24 hours ago
                    pass

    Series['hours_since_published'] = round((now_unix - publish_date_unix) / 3600)
    #print(Series)
    return(Series)


def fill_empty_velocity(DataFrame, velocity_columns):
    for item in velocity_columns:
        DataFrame[item].replace(to_replace=[0, '0',None], value=-1, inplace=True)
    return DataFrame


def fill_na_with_mean(DataFrame, numerical_columns):
    for item in numerical_columns:
        DataFrame[item].replace(to_replace=[0, '0',None], value=DataFrame[item].mean(), inplace=True)
    return DataFrame

def standarize_numerical_colunms(DataFrame, numerical_columns, method='robust'):
    if method not in ('normal', 'robust', 'boxcox', 'minmax'):
        print('Wrong method')
        print('Avilable methods are: normal, robust, boxcox, minmax')
        return DataFrame
    from sklearn.preprocessing import RobustScaler
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.preprocessing import StandardScaler
    from scipy.stats import boxcox
    epsilon = 0.000001
    for item in numerical_columns:
        if method=='boxcox':
            if item in ('distance_from_images_array', 'distance_from_publisher_spot_array', 'distance_from_paragraphs_array', 'distance_from_social_media_widgets_array', 'distance_from_content_recommendations_array'):
                continue
            DataFrame[item]=boxcox(DataFrame[item]+epsilon)[0] #box cox the data
        elif method=='robust':
            DataFrame[item]=RobustScaler(quantile_range=(25, 75)).fit_transform(DataFrame[item].values.reshape(-1,1)) #standarize by quantiles
        elif method=='minmax':
            DataFrame[item]= MinMaxScaler(feature_range=(0,1)).fit_transform(DataFrame[item].values.reshape(-1,1))
        else:
            DataFrame[item] = StandardScaler(with_mean=True, with_std=True).fit_transform(DataFrame[item].values.reshape(-1, 1))
    return DataFrame

def add_viewability_to_features(DataFrame, viewability_columns=['viewability_per_user_ad_index_0','viewability_per_user_ad_index_1',
                                                                'viewability_per_url_ad_index_0','viewability_per_url_ad_index_1']):
    DataFrame['viewability_per_user'] = np.where(DataFrame['ad_index']== 0, DataFrame[viewability_columns[0]], DataFrame[viewability_columns[1]])
    DataFrame['viewability_per_url'] = np.where(DataFrame['ad_index'] == 0, DataFrame[viewability_columns[2]], DataFrame[viewability_columns[3]])
    DataFrame.drop(viewability_columns, axis=1, inplace=True)
    DataFrame['viewability_per_user']= DataFrame['viewability_per_user'].astype('float64')
    DataFrame['viewability_per_url']= DataFrame['viewability_per_url'].astype('float64')
    return DataFrame



def drop_unwanted_columns(DataFrame,drop_column_list):
    DataFrame.drop(drop_column_list, axis=1, inplace=True)
    return DataFrame

def remove_remaining_na(DataFrame):
    DataFrame.dropna(axis=0, how='any', inplace=True)
    return DataFrame

def add_dummy_columns(DataFrame):
    for col in categorical_columns:
        col_dummies = pd.get_dummies(DataFrame[col])
        DataFrame = pd.concat([DataFrame, col_dummies], axis=1)
        del DataFrame[col]
    return DataFrame

def add_missing_dummy_columns(DataFrame, geo_columns=['ROW', 'br', 'gb', 'us'], ts_columns=['default', 'search', 'social'], browser_columns=['Other Browser', 'chrome', 'safari'],
                              hour_category_columns=['evening', 'morning', 'night', 'noon']):
    for column in geo_columns:
        if column not in DataFrame.columns:
            DataFrame[column] = 0
    for column in ts_columns:
        if column not in DataFrame.columns:
            DataFrame[column] = 0

    for column in browser_columns:
        if column not in DataFrame.columns:
            DataFrame[column] = 0

    for column in hour_category_columns:
        if column not in DataFrame.columns:
            DataFrame[column] = 0

    return DataFrame

def split_test_train(DataFrame, include_validation=True):
    import numpy as np
    if include_validation==True:
        train, validate, test = np.split(DataFrame.sample(frac=1), [int(.6 * len(DataFrame)), int(.8 * len(DataFrame))])
        return train, validate, test
    else:
        train, test=np.split(DataFrame.sample(frac=1), [int(.666 * len(DataFrame))])
        return train,  test

def split_X_Y(DataFrame, target='is_viewed'):
    X = DataFrame.ix[:, DataFrame.columns != target]
    Y = DataFrame[target]
    return X, Y


def PCA(X):
    from sklearn.decomposition import PCA
    pca = PCA(svd_solver='randomized', n_components=5, whiten=True)
    X_new = pca.fit_transform(X)
    headers=[]
    for i in range(len(X_new[0])):
        headers.append('PCA_'+str(i+1))
    X_new_DF=pd.DataFrame(data=X_new, columns=headers)
    X_new_DF.index=X.index #Though the indices are removed by PCA but the underlying order of rows remains(see implementation for the transform function of PCA*). So it is safe to have df2.index = df1.index
    return  X_new_DF

def balance_data(X,Y, type='combined', target='is_viewed'):
    headers=X.columns
    if type not in ('under', 'over', 'combined'):
        print('legal values for type are under, over, combined')
        return X, Y
    if type=='under':
        from imblearn.under_sampling import EditedNearestNeighbours
    #applies a nearest-neighbors algorithm and “edit” the dataset by removing samples which do not agree “enough” with their neighboorhood.
    # For each sample in the class to be under-sampled, the nearest-neighbours are computed and if the selection criterion is not fulfilled,
    # the sample is removed. all the nearest-neighbors have to belong to the same class of the sample inspected to keep it in the dataset
        enn = EditedNearestNeighbours(ratio='majority', n_neighbors=7, kind_sel='mode')
        X_res, y_res = enn.fit_sample(X, Y)

        return pd.DataFrame(data=X_res, columns=headers), pd.DataFrame(y_res, columns=[target])
    elif type=='over':
        from imblearn.over_sampling import SMOTE
    # SMOTE=  Synthetic Minority Over-Sampling Technique
    # The idea is to find the minoruty class and to generate new data. the data takes a samplle from the minority calss and finds the  K nearest neighbours from the dataset.
    # This algorithm classifies the samples of minority classes into 3 distinct groups – Security/Safe samples, Border samples, and latent nose samples. This is done by calculating the distances among samples of the minority class and samples of the training data.
    # Security samples are those data points which can improve the performance of a classifier. While on the other hand, noise are the data points which can reduce the performance of the classifier.  The ones which are difficult to categorize into any of the two are classified as border samples.
        sm = SMOTE(ratio='minority', kind='borderline1', k_neighbors=5, m_neighbors=20)
        X_res, y_res = sm.fit_sample(X, Y)
        return pd.DataFrame(data=X_res, columns=headers), pd.DataFrame(y_res, columns=[target])
    else:
        from imblearn.combine import SMOTEENN
    #We previously presented SMOTE and showed that this method can generate noisy samples by interpolating new points between marginal outliers and inliers.
    # This issue can be solved by cleaning the resulted space obtained after over-sampling.
    # What it does, essentialy is to oversample and then clean noise (undersample)
        sme = SMOTEENN(ratio='all')
        X_res, y_res = sme.fit_sample(X, Y)
        return pd.DataFrame(data=X_res, columns=headers), pd.DataFrame(y_res, columns=[target])

def recombine(X,Y):
    DataFrame = pd.concat([X, Y], axis=1)
    return DataFrame






