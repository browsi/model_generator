# coding: utf-8
def Predict(X,clf):
    return clf.predict(X)

def Predict_proba(X,clf):
    return clf.predict_proba(X)[:,1]



def ROC_curve(Y, Y_proba):
    from sklearn import metrics
    from matplotlib import pyplot as plt
    import config
    fpr, tpr, tresholds = metrics.roc_curve(y_true=Y, y_score=Y_proba,
                                                drop_intermediate=True)  # calculate ROC and drop results that are below the diagonal

    auc = metrics.roc_auc_score(y_true=Y, y_score=Y_proba,
                                    average='weighted')  # calculate auc per calss 0 and class 1 and calculate the total score by giving the weighted mean (weight= number of actual results per class)
    auc = round(auc, 3)
    plt.plot(fpr, tpr, label=" auc=" + str(auc))
    diagonal = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    plt.plot(diagonal, diagonal, '--', linewidth=1, c='grey')
    plt.xticks(rotation=90)
    plt.xlabel('False Positive Rate= FP/(FP+TN)')
    plt.ylabel('True Positive Rate= TP/(TP+FN)')
    plt.title('ROC curve')
    plt.ylim((-0.001, 1.001))
    plt.xlim((-0.001, 1.001))
    plt.legend(loc="lower right")
    plt.savefig("ROC_curve_for_"+config.site_key+".png")
    return plt
def gen_classification_report(Y, Y_predict):

    from sklearn import metrics
    classification_report = metrics.classification_report(y_true=Y, y_pred=Y_predict,
                                                              target_names=['class 0- unviewed ad',
                                                                            'class 1- viewed ad'])
        # f1 = metrics.f1_score(y_true=Y, y_pred=yhats, labels=None, pos_label=1,
        #                     average='weighted')  # returns F1 result only for Class=1 - "viewed ad" (since we have a binary calssification problem)
        # Precision= tp/(tp+fp)--> I preidcited '1'. how many out of these predictions are really '1'.
        # Recall= tp/(tp+fn)---> How much out of all of the '1' have I managed to predict.
    print('Precision (or TPR)= TP/(TP+FP)= if I predicted "1"  how accruate is my prediction?')
    print('Recall (sensitivity)= TP/(TP+FN)= given "1" how many did I catch?')
    print('F1 is the harmonic mean of precision and recall multiplied by 2- in order to get a score brtween 0-1')
    print('F1 = 2*(precision*recall)/(precision+recall)')

    return classification_report

def log_loss(Y, Y_proba):
    from sklearn import metrics
    log_loss = metrics.log_loss(y_true=Y, y_pred=Y_proba, eps=10 ** (-10), normalize=True,
                                    labels=None)  # calculate log loss, eps is given since log is undifined for P=0 or P=1, normalize gives us the log loss per samle (division by N) instead of the total logloss
    return log_loss

def accuracy(Y, Y_predict):

        from sklearn import metrics
        acc = metrics.accuracy_score(y_true=Y, y_pred=Y_predict, normalize=True)  # return the accuracy. the normalize=True mentiones that the results will be returned as a ratio of correct prediction instead of the number of correct prediction.
        return round(acc, 5)


def auc(Y, Y_proba):
    from sklearn import metrics
    auc = metrics.roc_auc_score(y_true=Y, y_score=Y_proba,average='weighted')  # calculate auc per calss 0 and class 1 and calculate the total score by giving the weighted mean (weight= number of actual results per class)
    return round(auc, 3)



def brier_score(Y, Y_proba):  # (1/N) *SIGMA (Yi_proba-Yi_true)^2 .
    from sklearn.metrics import brier_score_loss
    brier = brier_score_loss(Y, Y_proba)
    return brier


def reliability_curve(Y, Y_proba, bins=10):
    import config
    from matplotlib import pyplot as plt
    from sklearn.calibration import calibration_curve
    """Compute reliability curve

    Reliability curves allow checking if the predicted probabilities of a
    binary classifier are well calibrated. This function returns two arrays
    which encode a mapping from predicted probability to empirical probability.
    For this, the predicted probabilities are partitioned into equally sized
    bins and the mean predicted probability and the mean empirical probabilties
    in the bins are computed. For perfectly calibrated predictions, both
    quantities whould be approximately equal (for sufficiently many test
    samples).

    Note: this implementation is restricted to binary classification.

    Parameters
    ----------

    Y : array, shape = [n_samples]
        True binary labels (0 or 1).

    Y_proba : array, shape = [n_samples]
        Target scores, can either be probability estimates of the positive
        class or confidence values. If normalize is False, Y_proba must be in
        the interval [0, 1]

    bins : int, optional, default=10
        The number of bins into which the Y_probas are partitioned.
        Note: n_samples should be considerably larger than bins such that
              there is sufficient data in each bin to get a reliable estimate
              of the reliability



    Returns
    -------
    prob_pred : array, shape = [bins]
        The mean predicted Y_proba in the respective bins.

    prob_true : array, shape = [bins]
        The empirical probability (frequency) of the positive class (+1) in the bin


    """


    prob_true,prob_pred=calibration_curve(Y, Y_proba, normalize=False, n_bins=bins)
    print(prob_true)
    print(prob_pred)

    # Plot Reliability curve
    plt.figure(0, figsize=(8, 8))
    plt.subplot2grid((3, 1), (0, 0), rowspan=2)
    plt.plot([0.0, 1.0], [0.0, 1.0], 'k', label="Perfect")

    plt.plot(prob_pred,
             prob_true)
    plt.ylabel("Empirical probability")
    plt.legend(loc=0)

    #Plot the probabilities Hist
    plt.subplot2grid((3, 1), (2, 0))

    plt.ylabel("Count")
    plt.hist(Y_proba, range=(0, 1), bins=bins,
             histtype="bar", lw=2)
    plt.xlabel("Predicted Probability")
    plt.legend(loc='upper center', ncol=2)


    plt.savefig("Reliabiliy_curve_for"+config.site_key+"_calbiration_"+config.calibration_method+".png")

    return plt


