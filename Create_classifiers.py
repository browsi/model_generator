# coding: utf-8
def KNN(X,Y, params={'n_neighbors': 5, 'weights': 'distance'}):
    from sklearn.neighbors import KNeighborsClassifier
    import config
    neigh = KNeighborsClassifier(n_neighbors=params['n_neighbors'], weights=params['weights'])
    if config.calibrate:
        from sklearn.calibration import CalibratedClassifierCV
        neigh_cal=CalibratedClassifierCV(base_estimator=neigh, method=config.calibration_method, cv=3)
        neigh_cal.fit(X,Y)
        return neigh_cal
    else:
        neigh.fit(X,Y)
        return neigh

    

def NaiveBayes(X,Y, categorical_columns, numeric_columns, params={'alpha':1, 'fit_prior': True, 'binarize': None}):
    from sklearn.naive_bayes import GaussianNB
    from sklearn.naive_bayes import BernoulliNB
    gnb = GaussianNB().fit(X[numeric_columns],Y)
    if len(categorical_columns)>=1: # if we use PCA then all columns are numerical
        clf = BernoulliNB(alpha=params['alpha'], fit_prior=params['fit_prior'], binarize=params['binarize']).fit(X[categorical_columns],Y)
        return gnb, clf
    else:
        return gnb, None

def GardientBoostedRandomForest(X,Y, params={'objective':'binary:logistic', 'silent':False, 'missing':None, 'nthread': -1, 'n_estimators':1000, 'learning_rate':0.01,
                                             'max_depth':10, 'gamma': 3, 'colsample_bytree': 0.6, 'subsample':0.7, 'reg_lambda':1,  'min_child_weight':5, 'max_delta_step':5, 'scale_pos_weight': 1}):
    from xgboost import XGBClassifier
    import config
    xgb= XGBClassifier(objective=params['objective'], silent=params['silent'], missing=params['missing'], nthread=params['nthread'],n_estimators=params['n_estimators'],
                       learning_rate=params['learning_rate'], max_depth=params['max_depth'], gamma=params['gamma'], colsample_bytree=params['colsample_bytree'],
                       subsample=params['subsample'],reg_lambda=params['reg_lambda'], min_child_weight=params['min_child_weight'], max_delta_step=params['max_delta_step'], 
                       scale_pos_weight=params['scale_pos_weight'])
    if config.calibrate:
        from sklearn.calibration import CalibratedClassifierCV
        xgb_cal=CalibratedClassifierCV(base_estimator=xgb, method=config.calibration_method, cv=3)
        xgb_cal.fit(X,Y)
        return xgb_cal
    else:
        xgb.fit(X,Y)
        return xgb

def CostSensitiveRandomForest(X,Y,params={'n_estimators':100, 'combination':'majority_bmr', 'max_features':None, 'n_jobs':-1, 'pruned':True,
                                          'costs':[1,10,0,0]}):# cost matrix FP/FN/TP/TN
    from costcla.models import CostSensitiveRandomForestClassifier
    import numpy as np
    X = X.as_matrix(columns=None)
    Y = Y.as_matrix(columns=None)
    #create cost matrix for all samples in X
    costs = np.array(params['costs'])
    cost_matrix=np.tile(costs, (X.shape[0], 1))
    csrf=CostSensitiveRandomForestClassifier(n_estimators=params['n_estimators'], combination=params['combination'],
                                             max_features=params['max_features'], n_jobs=params['n_jobs'], pruned=params['pruned'])
    csrf.fit(X,Y,cost_matrix)
    return csrf

def RandomForest(X,Y, params={'oob_score': True , 'bootstrap':'True', 'criterion': 'gini', 'n_estimators':500, 'max_features': None, 'max_depth': 10, 'max_leaf_nodes': 500, 'min_impurity_decrease': 0.15,
        'min_samples_leaf':0.05, 'min_samples_split': 0.05, 'class_weight':{0:1.055, 1: 1},   'random_state':None, 'verbose':1}):
    from sklearn.ensemble import RandomForestClassifier
    rf = RandomForestClassifier(class_weight=params['class_weight'], oob_score=params['oob_score'],
                                bootstrap=params['bootstrap'], criterion=params['criterion'],
                                random_state=params['random_state'], n_estimators=params['n_estimators'],
                                max_features=params['max_features'], max_depth=params['max_depth'],
                                max_leaf_nodes=params['max_leaf_nodes'],
                                min_impurity_decrease=params['min_impurity_decrease'],
                                min_samples_leaf=params['min_samples_leaf'],
                                min_samples_split=params['min_samples_split'], verbose=params['verbose']).fit(X,Y)
    return rf



def ExtremeRandomForest(X,Y, params={'oob_score': True , 'bootstrap':'True', 'criterion': 'entropy', 'n_estimators':1000, 'max_features': None, 'max_depth': 10, 'max_leaf_nodes': 200, 'min_impurity_decrease': 0.45,
        'min_samples_leaf':0.01, 'min_samples_split': 0.01, 'class_weight':'balanced',   'random_state':None}):
    from sklearn.ensemble import ExtraTreesClassifier
    xrf = ExtraTreesClassifier(class_weight=params['class_weight'], oob_score=params['oob_score'],
                                bootstrap=params['bootstrap'], criterion=params['criterion'],
                                random_state=params['random_state'], n_estimators=params['n_estimators'],
                                max_features=params['max_features'], max_depth=params['max_depth'],
                                max_leaf_nodes=params['max_leaf_nodes'],
                               min_impurity_decrease=params['min_impurity_decrease'],
                                min_samples_leaf=params['min_samples_leaf'],
                                min_samples_split=params['min_samples_split']).fit(X,Y)
    return xrf

def SVC(X,Y, params={'C': 7 , 'kernel':'rbf', 'gamma': 0.001, 'probability':True, 'shrinking': True, 'tol': 0.001,
                              'class_weight':'balanced', 'max_iter':1000,  'random_state':None}):
    from sklearn.svm import SVC
    svc = SVC(C=params['C'], kernel=params['kernel'], gamma=params['gamma'], probability=params['probability'],
              shrinking=params['shrinking'], tol=params['tol'],
              class_weight=params['class_weight'], max_iter=params['max_iter'], random_state=params['random_state'])
    svc.fit(X,Y)
    return svc


def Logistic_Regression(X,Y, params={'penalty':'l2', 'C': 0.2, 'class_weight': 'balanced', 'max_iter':10000, 'n_jobs':-1, 'solver':'saga'}):
    from sklearn.linear_model import LogisticRegression
    lr=LogisticRegression(penalty=params['penalty'], C=params['C'], class_weight=params['class_weight'],
                          max_iter=params['max_iter'], n_jobs=params['n_jobs'], solver=params['solver'])
    lr.fit(X,Y)
    return lr


def GridSerach(classifier, params_dict, X, Y):
    from sklearn.model_selection import GridSearchCV
    if classifier=='knn':
        from sklearn.neighbors import KNeighborsClassifier
        knn=KNeighborsClassifier()
        gs = GridSearchCV(knn, params_dict[classifier], scoring='f1').fit(X,Y)
    elif classifier=='naive_bayes':
        from sklearn.naive_bayes import GaussianNB
        gnb=GaussianNB()
        gs = GridSearchCV(gnb, params_dict[classifier], scoring='f1').fit(X,Y)
    elif classifier=='xgboost':
        from xgboost import XGBClassifier
        xgb = XGBClassifier(learning_rate =0.1, n_estimators=1000)
        gs = GridSearchCV(xgb, params_dict[classifier], scoring='f1').fit(X,Y)
    elif classifier=='rf':
        from sklearn.ensemble import RandomForestClassifier
        rf=RandomForestClassifier()
        gs = GridSearchCV(rf, params_dict[classifier], scoring='f1').fit(X,Y)
    elif classifier=='xrf':
        from sklearn.ensemble import ExtraTreesClassifier
        xrf = ExtraTreesClassifier()
        gs = GridSearchCV(xrf, params_dict[classifier], scoring='f1').fit(X,Y)
    elif classifier=='svc':
        from sklearn.svm import SVC
        svc = SVC()
        gs = GridSearchCV(svc, params_dict[classifier], scoring='f1').fit(X,Y)
    elif classifier=='lr':
        from sklearn.linear_model import LogisticRegression
        lr = LogisticRegression()
        gs = GridSearchCV(lr, params_dict[classifier], scoring='f1').fit(X,Y)

    full_results=gs.cv_results_
    best_estimator=gs.best_estimator_
    best_params=gs.best_params_
    best_score=gs.best_score_

    return full_results,best_estimator,best_params,best_score
